%Discrete model definition based on Casadi: 
import casadi.*

%States:
Ov = MX.sym('Ov',1,1); 
Sv = MX.sym('Sv',1,1);
Sen = MX.sym('Sen',1,1);
beta = MX.sym('beta',1,1); 
Th = MX.sym('Th',1,1); 
Cop = MX.sym('Cop',1,1);
Dia = MX.sym('Dia',1,1); 
St_p = MX.sym('St_p',1,1);
Mood = MX.sym('Mood',1,1); 
x=[Ov;Sv;Sen;beta;Th;Cop;Dia;Mood];

%Inputs:
ucop = MX.sym('ucop',1,1); %The ability to change coping 
udia = MX.sym('udia',1,1); %The ability to change diatheses

u=[ucop;udia];

%Disturbance events: 
e = MX.sym('e',1,1);

%Parameters: 
Lt_p = MX.sym('lt_p',1,1);
Wsvm = MX.sym('Wsvm',1,1);
Wthm = MX.sym('Wthm',1,1);
Wsvth = MX.sym('Wsvth',1,1);
Wmth = MX.sym('Wmth',1,1);
Wmsen = MX.sym('Wmsen',1,1);
Wthsen = MX.sym('Wthsen',1,1); 

Param = [Lt_p;Wsvm;Wthm;Wsvth;Wmth;Wmsen;Wthsen];

%Time constant: 
dt = MX.sym('dt',1,1);

%The model: 

%For Ov:
deltamood = Mood - beta*Lt_p;

phi=if_else(deltamood>=0, Ov*deltamood, (1-Ov)*deltamood);

Ovn = Ov-Sen*phi*dt + e;

%For Sv
Gamma = Dia*Ov*Th+Cop*(1-(1-Ov))*(1-Th);

Svn = Sv + (Gamma - Sv) * dt;

%For Mood: 
psi = Sv*Wsvm + Th*Wthm;

Moodn = if_else(psi>=Mood, Mood + Cop*(psi-Mood)*dt, Mood + Dia*(psi-Mood)*dt);

%For Thoughts: 
psi = Sv*Wsvth + Mood*Wmth;

Thn = if_else(psi>=Th, Th + Cop*(psi - Th)*dt, Th + Dia*(psi-Th)*dt);

%For Sensitivity: 
psi = Mood*Wmsen + Th*Wthsen;

Senn = if_else(psi>=Sen, Sen + Cop*(psi-Sen)*dt, Sen + Dia*(psi-Sen)*dt);

%For beta:
betan = beta + ( Dia * (Mood/Lt_p - beta ) + Cop*(1-beta) )*dt;

St_pn = betan*Lt_p;

%For Cop:
Copn = Cop+ucop*dt;

%For Dia: 
Dian = Dia + udia*dt; 

xn=[Ovn;Svn;Senn;betan;Thn;Copn;Dian;Moodn];

%Define the discrete function dynamics: 
F=Function('F',{x,u,e,Param,dt},{xn},{'x','u','e','Param','dt'},{'xn'});
