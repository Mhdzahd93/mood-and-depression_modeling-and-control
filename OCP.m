%Define the optimal control problem:

%Define the model: 
DefineDiscModel;

%Define the parameter vector:
Paramdef; 

%Optimal control part: 
opti = casadi.Opti();

%OCP parameters: 
P = opti.parameter(size(Param,1),size(Param,2));
%initial conditions: 
X0 = opti.parameter(size(x,1),size(Param,2));
%Disturbance evens vector: 
E = opti.parameter(size(e,1),H);

%Define the control trajectory: 
U = opti.variable(size(u,1),H);
ucop = U(1,:); 
udia = U(2,:);

%Define the states: 
X = opti.variable(size(x,1),H+1); 
Ov = X(1,:);
Sv = X(2,:); 
Sen = X(3,:); 
beta = X(4,:);
Th = X(5,:);
Cop = X(6,:);
Dia = X(7,:);
Mood = X(8,:);

%Multiple shooting: 
opti.subject_to(X(:,1)==X0);

for k=1:H
   opti.subject_to( X(:,k+1) == F(X(:,k),U(:,k),E(:,k),P,dt));
end

%Define the cost: 
Cost = 10*ucop.^2 + 10*udia.^2 + (Mood(2:end)-beta(2:end)*Lt_p).^2;
opti.minimize(sum(Cost));
%Define Constraints: 

%State constraints: 
opti.subject_to(0<=Cop<=1);
opti.subject_to(0<=Dia<=1);

%Input constraints: 
opti.subject_to(-0.02<=ucop<=0.01);
opti.subject_to(-0.01<=udia<=0.02);

%Options for the solver:
%opti.solver('ipopt', struct("expand",true ))
opti.solver('ipopt', struct("expand",true), struct("tol", 1e-9, "max_iter", 10000))


opti.set_value(P, param);

opti.set_value(X0, x0);

opti.set_value(E, e);

%Solve: 
results = opti.solve_limited();
X = results.value(X);
U = results.value(U);

plotting;