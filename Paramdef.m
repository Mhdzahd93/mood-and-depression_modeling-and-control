%Define parameters and initial conditions: 

%Define Prediction horizon and time steps: 

%Prediction horizon: 

T_final = 200; %500 hours

%Step size:

dt = 1; %1 hour

%Number of steps: 
H = T_final/dt; 

%Define the parameters: 
Wsvm = 0.7;
Wthm = 0.3;
Wsvth = 0.6;
Wmth = 0.4;
Wmsen = 0.5;
Wthsen = 0.5;
e=zeros(1,H);
e(1) = -0.5;

%These parameters are for a person who is more valnurable to depression:

%Emotionaly very unstable person: 
Lt_p = 0.6;
param = [Lt_p;Wsvm;Wthm;Wsvth;Wmth;Wmsen;Wthsen];
%initial conditions for the emotionaly very unstable:
x0=[0.999; Lt_p; Lt_p; Lt_p; Lt_p; 0.01; 0.99; Lt_p];

%Emotionaly slightly unstable person: 
% Lt_p = 0.6;
% Param = [Lt_p;Wsvm;Wthm;Wsvth;Wmth;Wmsen;Wthsen];
% %initial conditions for the emotionaly very unstable:
% x0=[0.94; Lt_p; Lt_p; Lt_p; Lt_p; 0.1; 0.9; Lt_p];

%Normal person: 
% Lt_p = 0.8; 
% Param = [Lt_p;Wsvm;Wthm;Wsvth;Wmth;Wmsen;Wthsen];
% %initial conditions for the emotionaly very unstable:
% x0=[0.8; Lt_p; Lt_p; Lt_p; Lt_p; 0.5; 0.5; Lt_p];

