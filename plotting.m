%Plotting:
%time vector: 
time= 0:dt:T_final; 

%Ploting the states: 
figure
plot(time,X(1:5,:),'LineWidth',2)
hold on
plot(time,X(end,:))
xlabel('Time [Hours]','Interpreter','latex')
ylabel('States')
leg = legend('OEVS','SEVS','Sensitivity','Thoughts','Mood');
set(leg,'Interpreter','latex')
grid on

%Ploting the Coping and Diathesis: 
figure
plot(time,X(5:6,:),'LineWidth',2)
hold on
xlabel('Time [Hours]','Interpreter','latex')
ylabel('Coping and Diatheses')
leg = legend('Coping','Diatheses');
set(leg,'Interpreter','latex')
grid on

%Plotting the inputs: 
figure
plot(time(1:end-1),U,'LineWidth',2)
hold on
xlabel('Time [Hours]','Interpreter','latex')
ylabel('Inputs')
leg = legend('$u_{cop}$','$u_dia$');
set(leg,'Interpreter','latex')
grid on